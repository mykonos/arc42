# arc42

A sample project containing a arc42 documentation including a PlantUML and a SVG example
### Usage
Simply run `mvn`. The result `arc42-template.pdf` is located in `target/generated-docs`