ifndef::docroot[:docroot: ..]
[[section-glossary]]
== Glossar



[cols="e,2e" options="header"]
|===
|Begriff |Definition

|Lohnsteuererklärung
|Die Lohnsteuererklärung ist eine freiwillig von Arbeitnehmern mit Einkünften aus einer nicht selbstständigen Arbeit abgegebene Steuererklärung. Die Lohnsteuererklärung ist eine in den meisten Fällen freiwillige Steuererklärung und wird daher als Antragsveranlagung bezeichnet. Wird keine Steuererklärung abgegeben, gehen die Finanzbehörden davon aus, dass mit der vom Arbeitgeber abgeführten Lohnsteuer die Steuerschuld beglichen ist.

|Steuererklärung
|s. Lohnsteuererklärung
|===
